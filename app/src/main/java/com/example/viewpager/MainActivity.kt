package com.example.viewpager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager.adapter.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {


    private lateinit var viewPager: ViewPager2
    private lateinit var tabs: TabLayout
    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        viewPager.adapter = viewPagerFragmentAdapter

        TabLayoutMediator(tabs, viewPager){tab, position ->
            when (position){
                0 -> tab.text = "Note"
                1 -> tab.text = "ME"
                2 -> tab.text = "VINDIZELOVICHI"
            }
        }.attach()

    }

    private fun init(){

        viewPager = findViewById(R.id.viewPager)
        tabs = findViewById(R.id.tabs)
        viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)
    }
}