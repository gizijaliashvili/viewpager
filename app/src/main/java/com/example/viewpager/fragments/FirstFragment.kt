package com.example.viewpager.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager.R

class FirstFragment: Fragment(R.layout.fragment_first)  {

    private lateinit var noteEditText: EditText
    private lateinit var buttonAdd: Button
    private lateinit var textView: TextView
    private lateinit var sharedPreferences: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)

        sharedPreferences = requireActivity().applicationContext.getSharedPreferences("MY_APP_P", Context.MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTES", "")
        textView.text = text

        registerListener()
    }

    private fun init(view: View){
        noteEditText = view.findViewById(R.id.noteEditText)
        buttonAdd = view.findViewById(R.id.buttonAdd)
        textView = view.findViewById(R.id.textView)
    }
    private fun registerListener(){
        buttonAdd.setOnClickListener{
            val note = textView.text.toString()
            val text = noteEditText.text.toString()
            val result = note+"\n"+text
            textView.text = result
            sharedPreferences.edit()
                .putString("NOTES", result)
                .apply()
            noteEditText.text.clear()
        }
    }
}